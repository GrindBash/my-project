var express = require('express');
var router = express.Router();
var client = require('./connectBase');
//API метод для вывода списка отделов
client.connect(function(err, client,done) {
    router.get("/dep",  function(request,response) {
        if (err)
        { 
            throw err
        }
        client.query('select * from department;',[],function(err, result){
            if (err)
            {
                throw err
            }
            response.json(result.rows)
            
            client.end(err => {
                console.log('client has disconnected')
                if (err) {
                  console.log('error during disconnection', err.stack)
                }
              
            })
        })
    })

//API метод для вывода списка сотрудников
    router.get("/st",  function(request,response) {
        if (err)
        { 
            throw err
        }
        client.query('select * from stuff;',[],function(err, result){
            if (err)
            {
                throw err
            }
            response.json(result.rows)
            client.end(err => {
                console.log('client has disconnected')
                if (err) {
                  console.log('error during disconnection', err.stack)
                }
            })
        })
    })

//API метод для создания отдела
    router.post("/dep",  function(request,response) {
        if (err)
        { 
            throw err
        }
        var table = request.body;
        client.query('insert into department (dep_name,dep_description) values ($1,$2);',[table.dep_name, table.dep_description],function(err, result){
            if (err)
            {
                throw err
            }
            console.log('Department war created!');
            response.json(result.rows)
            client.end(err => {
                console.log('client has disconnected')
                if (err) {
                  console.log('error during disconnection', err.stack)
                }
            })
        })
    })

//API метод для редактирование отдела
    router.put("/dep",  function(request,response) {
        if (err)
        { 
            throw err
        }
        var table = request.body;   
        client.query('update department set dep_name = ($1), dep_description = ($2) where dep_id = ($3);', [table.dep_name, table.dep_description, table.dep_id], function(err, result){
            if (err)
            {
                throw err
            }
            console.log('Department war updated!');
            response.json(result.rows[0])
            client.end(err => {
                console.log('client has disconnected')
                if (err) {
                  console.log('error during disconnection', err.stack)
                }
            })
        })
    })

//API метод для для вывода данных отдела по его ID
    router.get("/dep/:id",  function(request,response) {
        if (err)
        { 
            throw err
        }
        var id = parseInt(request.params.id)
        client.query('select * from department where dep_id = $1;', [id], function(err, result){
            if (err)
            {
                throw err
            }
            console.log('Department show details!');
            response.json(result.rows)
            client.end(err => {
                console.log('client has disconnected')
                if (err) {
                  console.log('error during disconnection', err.stack)
                }
            })
        })
    }) 

//API метод для удаления отдела
    router.delete("/dep",  function(request,response) {
        if (err)
        { 
            throw err
        }
        var table = request.body;
        client.query('delete from department where dep_name = $1;',[table.dep_name],function(err, result){
            if (err)
            {
                throw err
            }
            console.log('Department ' + table.dep_name +'was deleted!');
            response.json(result.rows)
            client.end(err => {
                console.log('client has disconnected')
                if (err) {
                  console.log('error during disconnection', err.stack)
                }
            })
        })
    })

//API метод для создания сотрудника
    router.post("/st",  function(request,response) {
        if (err)
        { 
            throw err
        }
        var table = request.body;
        client.query('insert into stuff (st_surname,st_name,st_patronomic,st_birthday,st_position) values ($1,$2,$3,$4,$5);',[table.st_surname, table.st_name,table.st_patronomic,table.st_birthday,table.st_position],function(err, result){
            if (err)
            {
                throw err
            }
            console.log('Stuff ' + table.st_surname +' was created!');
            response.json(result.rows)
            client.end(err => {
                console.log('client has disconnected')
                if (err) {
                  console.log('error during disconnection', err.stack)
                }
            })
        })
    })

//API метод для удаления сотрудника
    router.delete("/st",  function(request,response) {
        if (err)
        { 
            throw err
        }
        var table = request.body;
        client.query('delete from stuff where st_name = $1;',[table.st_name],function(err, result){
            if (err)
            {
                throw err
            }
            console.log('Stuff ' + table.st_name +' was deleted!');
            response.json(result.rows)
            client.end(err => {
                console.log('client has disconnected')
                if (err) {
                  console.log('error during disconnection', err.stack)
                }
            })
        })
    })

//API метод для для вывода данных сотрудника по его ID
    router.get("/st/:id",  function(request,response) {
        if (err)
        { 
            throw err
        }
        var id = parseInt(request.params.id)
        client.query('select * from stuff where st_id = $1;', [id], function(err, result){
            if (err)
            {
                throw err
            }
            console.log('Stuff show details!');
            response.json(result.rows)
            client.end(err => {
                console.log('client has disconnected')
                if (err) {
                  console.log('error during disconnection', err.stack)
                }
            })
        })
    }) 

//API метод для редактирование сотрудника
    router.put("/st",  function(request,response) {
        if (err)
        { 
            throw err
        }
        var table = request.body;   
        client.query('update stuff set st_surname = ($1), st_name = ($2), st_patronomic = ($3), st_birthday = ($4), st_position = ($5) where st_id = ($6);', [table.st_surname, table.st_name,table.st_patronomic,table.st_birthday,table.st_position, table.st_id], function(err, result){
            if (err)
            {
                throw err
            }
            console.log('Stuff war updated!');
            response.json(result.rows[0])
            client.end(err => {
                console.log('client has disconnected')
                if (err) {
                  console.log('error during disconnection', err.stack)
                }
            })
        })
    })

})



module.exports = router;
